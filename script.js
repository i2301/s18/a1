
let trainer = {};


trainer.name = `Ash Ketchum`
trainer.age = 10
trainer.pokemon = [`Pikachu`, `Charizard`]
trainer.friends = {
	kanto: [`Brock`, `Misty`],
	hoenn: [`May`, `Max`]
}

trainer.talk = () => {
	console.log(`Pikachu I choose you!`)
}


console.log(trainer)


/
console.log(trainer.name)

console.log(trainer["age"])


console.log(trainer.talk())


function Pokemon(name, lvl){
	this.name = name;
	this.level = lvl;
	this.health = lvl * 3
	this.attack = lvl
	this.intro = function(opponent){
		

		console.log(`Hi I'm ${this.name} and this is ${opponent.name}`)
	};

	this.tackle = (target) => {
		// console.log(target)	//object


		console.log(target.health -= this.attack)
		const targetHealth = target.health -= this.attack

		if(targetHealth <= 0){
			target.faint()
		}

	};
	this.faint = () => {

		console.log(`${this.name} has fainted`);
	}
}


let Pikachu = new Pokemon("Pikachu", 12)
console.log(Pikachu)

let Geodude = new Pokemon("Geodude", 8)
console.log(Geodude)


let Mewtwo = new Pokemon("Mewtwo", 100)
console.log(Mewtwo)


Geodude.tackle(Pikachu)
Pikachu.tackle(Geodude)


